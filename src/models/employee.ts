import * as Knex from 'knex';
export class EmployeeyModel {

  async select(db: Knex) {
    return await db('employee')
  }

  async insert(db: Knex, info: any) {
    console.log(info);
    
    return await db('employee')
      .insert(info)
  }

  async update(db: Knex, id: any, info: any) {
    return await db('employee')
      .where('id', id)
      .update(info)
  }

  async delete(db: Knex, id: any) {
    return await db('employee')
      .where('id', id)
      .delete()
  }

  async selectMaxId(db: Knex) {
    const sql:any = `SELECT MAX(id) as max_id FROM employee`;
    const data:any = await db.raw(sql);
    // console.log(data.rows[0]);
    
    return data.rows[0]
  }

}

