import * as crypto from 'crypto'

import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { LoginModel } from '../models/login'
import { log } from 'console'

export default async function login(fastify: FastifyInstance) {

  const db: any = fastify.pg
  const loginModel = new LoginModel()

  fastify.post('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body

    console.log(body);
    
    const username = body.username
    const password = body.password

    try {
      const encPassword = crypto.createHash('md5').update(password).digest('hex')
      const rs: any = await loginModel.login(db, username, encPassword)
      const date = new Date();
      const iat = Math.floor(date.getTime() / 1000);
      const exp = Math.floor((date.setDate(date.getDate() + 7)) / 1000);

      if (rs.length > 0) {
        const user: any = rs[0]
        const info:any = {
          id    : user.user_id,
          name  : user.username,
          email : 'thawatchai.sea2@gmail.com',
          avatar: 'assets/images/avatars/brian-hughes.jpg',
          status: 'online'

        }
        const payload: any = {
          iat: iat,
          iss: 'Fuse',
          exp: exp
        }

        const token = fastify.jwt.sign(payload)
        reply.send({ accessToken : token , user : info , tokenType : 'bearer'})
      } else {
        reply.status(401).send({ ok: false, message: 'Login failed' })
      }


    } catch (error:any) {
      reply.status(500).send({ message: error.message })
    }
  })

}
