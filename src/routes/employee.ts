import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'

import { EmployeeyModel } from '../models/employee'


const fromImportModel = new EmployeeyModel();
export default async function CocAnswer(fastify: FastifyInstance) {
const db = fastify.pg;
  
fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
  reply.send({ ok: true, message: 'Welcome to SmartRefer Clinent!'});
});

fastify.get('/select', async (request: FastifyRequest, reply: FastifyReply) => {
  const req:any = request;

  try {
    let res_: any = await fromImportModel.select(db);
    reply.send(res_);
  } catch (error) {
    reply.send({ ok: false, error: error });
  }
});

fastify.post('/insert', async (request: FastifyRequest, reply: FastifyReply) => {
  const req:any = request;

  let info = req.body.rows
  
  try {
    let res_maxid:any = await fromImportModel.selectMaxId(db);
    let max_id:any = res_maxid.max_id + 1;  
      
    info.id = max_id;
    let res_: any = await fromImportModel.insert(db, info);
    reply.send(res_);
  } catch (error) {
    reply.send({ ok: false, error: error });
  }
});

//update?allergy_id=xxx
fastify.put('/update', async (request: FastifyRequest, reply: FastifyReply) => {
  const req:any = request;

  let id = req.query.id
  let info = req.body.rows
  try {
    let res_: any = await fromImportModel.update(db, id, info);
    reply.send(res_);
  } catch (error) {
    reply.send({ ok: false, error: error });
  }
});

//delete?id=xxx
fastify.delete('/delete',  async (request: FastifyRequest, reply: FastifyReply) => {
  const req:any = request;

  let id = req.query.id
  try {
    let res_: any = await fromImportModel.delete(db, id);
    reply.send(res_);
  } catch (error) {
    reply.send({ ok: false, error: error });
  }
});

}