/*
 Navicat Premium Data Transfer

 Source Server         : Server-Local
 Source Server Type    : MySQL
 Source Server Version : 100605
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 100605
 File Encoding         : 65001

 Date: 31/01/2023 13:50:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(12) DEFAULT NULL,
  `lastname` varchar(12) DEFAULT NULL,
  `dateStart` date DEFAULT NULL,
  `dateEnd` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of employee
-- ----------------------------
BEGIN;
INSERT INTO `employee` (`id`, `firstname`, `lastname`, `dateStart`, `dateEnd`) VALUES (1, 'Dencha', 'kosa', '2022-05-01', '2022-05-02');
INSERT INTO `employee` (`id`, `firstname`, `lastname`, `dateStart`, `dateEnd`) VALUES (2, 'Tahwaqthcai', 'san', '2022-06-01', '2022-06-05');
INSERT INTO `employee` (`id`, `firstname`, `lastname`, `dateStart`, `dateEnd`) VALUES (3, 'Phon', 'J', '2022-07-01', '2022-07-05');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` varchar(13) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `is_accept` char(1) DEFAULT '0',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1026 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` (`user_id`, `cid`, `username`, `password`, `is_accept`) VALUES (1, '1234567890123', 'admin', '383bdcdb33047bf9a8a2bd11f0055d36', '0');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
